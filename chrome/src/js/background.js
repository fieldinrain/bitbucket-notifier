var repos = null;
var settings = null;
var version = null;
var unread = 0;
var intervalsSet = false;

$(document).ready(function(){
	loadData ();
	initBackground ();	
});

// Load data from storage.sync
function loadData() {
	chrome.storage.local.get(["version", "settings", "repos"], function(result) {
		version = result["version"];
        settings = result["settings"];
        repos = result["repos"];

        // Settings aren't filled in yet
        if (settings == null) 
			chrome.browserAction.setBadgeText({text: "!"});
		
		else {
			if (version < 1.4)
        		convert13To14 ();
        	if (version < 1.52)
        		convertTo152 ();

			settings.password = atob (settings.password);
			update ();
			initBackground ();
		}
    });
}

function convert13To14 () {
	console.log ("Updating old saved v1.3 data to v1.4");

	$.each(repos, function(index, repo) {
  		repo.user = settings.user;
	});

	chrome.storage.local.set({"version" : 1.4, "repos" : repos}, function() {});
}

function convertTo152 () {
	console.log ("Updating old saved data to v1.5.2");

	$.each(repos, function(index, repo) {
  		repo.last_notification = 0;
	});

	chrome.storage.local.set({"version" : 1.52, "repos" : repos}, function() {});
}

// Initialise intervals if data is loaded
function initBackground () {
	if (settings == null || intervalsSet)
		return;

	intervalsSet = true;
	setInterval (update, 20000);
	setInterval (updateIgnores, 35000);
	setNotificationListener ();
}

function setNotificationListener () {
	// Notification is clicked
	chrome.notifications.onClicked.addListener(function (id) {
		chrome.notifications.clear(id, function callback() {});
		chrome.tabs.create({'url': "https://bitbucket.org/" + id});

		// Get repo from id
		repo = getRepoFromNotificationId(id)
		if (repo == null)
			return;

		// Mark as read
		if (repo.notifs.length > 1)
			removeAllCommits(repo)
		else
			removeCommits(repo.notifs, repo)
	});

	// Mark as read is clicked
	chrome.notifications.onButtonClicked.addListener(function callback(id, index) {
		// Get repo from id
		repo = getRepoFromNotificationId(id)
		if (repo == null)
			return;
		
		// Mark as read
		removeCommits(repo.notifs, repo)
	});
}

// Fetch data from each repository
function update() {
	console.log ("Updating repositories");
	$.each(repos, function(index, repo) {
  		fetchRepository (repo);
	});
}

// Fetch data from repository
function fetchRepository (repo) {
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'https://bitbucket.org/api/2.0/repositories/' + repo.user + '/' + repo.name + '/commits/?t=' + Math.random());
	xhr.setRequestHeader("Authorization", "Basic " + btoa(settings.user + ":" + settings.password));
	xhr.responseType = 'json';
	
	xhr.onload = function(e) {
		repo.e = e;
		updateRepository (repo);
	};
	
	xhr.send();
}

// Update repository
function updateRepository (repo) {
	var total = 0;
	var commits = [];
	var notifs = [];

	if (repo.last_update == 0 && repo.e.srcElement.response.values.length > 0) {
		repo.last_update = new Date(repo.e.srcElement.response.values[0].date).getTime() / 1000 - 60;
		repo.last_notification = repo.last_update;
	}

	$.each(repo.e.srcElement.response.values, function(index, value) {
		var date = new Date(value.date).getTime() / 1000;
		
		if (date > repo.last_update && ignoreIndex(value.hash, repo) == -1) {
			total += 1;

			if (total <= settings.newShownLimit) {
				commits.push ({"message" : value.message, "hash" : value.hash, "date" : date });
			}

			if (repo.last_notification < date) {
				notifs.push ({"message" : value.message, "hash" : value.hash, "date" : date });
			}
		} 
	});

	repo.commits = commits;
	repo.unread = total;
	repo.notifs = notifs
	updateTotal();

	// Notifications
	sendNotifications (repo, notifs);

	// Save data
	chrome.storage.local.set({"repos" : repos}, function() {});
}

function sendNotifications (repo) {
	commits = repo.notifs

	if (commits.length > 1) {
		var opt = {
			type: "basic",
			title: commits.length + " new commits in " + repo.display_name,
			message: commits[0].message + ", ...",
			iconUrl: "img/bitbucket-128.png",
			buttons: [{
				title: "Mark as read", 
				iconUrl: "img/mark_notification.png"
			}]
		}

	    var id = repo.user + "/" + repo.name;
		chrome.notifications.create(id, opt, function callback(id) {
			removeNotification (id);
		});

	} else if (commits.length == 1) {
		var opt = {
			type: "basic",
			title: "New commit in " + repo.display_name,
			message: commits[0].message,
			iconUrl: "img/bitbucket-128.png",
			buttons: [{
				title: "Mark as read", 
				iconUrl: "img/mark_notification.png"
			}]
	    }
		
		var id = repo.user + "/" + repo.name + "/commits/" + commits[0].hash;
		chrome.notifications.create(id, opt, function callback(id) {
			removeNotification (id);
		});
	}

	$.each(commits, function(index, commit) {
		if (commit.date > repo.last_notification)
			repo.last_notification = commit.date;
	});
}

function getRepoFromNotificationId (id) {
	// Find repo
	idSplit = id.split ("/");
	repo = findRepo (idSplit[0], idSplit[1]);
	if (repo == undefined)
		return null;

	return repo
}

function removeNotification (id) {
	setTimeout(function() { 
		chrome.notifications.clear(id, function callback() {});
	}, 10000);
}

// What is the index of a commit in the ignore array? -1 if isn't in ignore
function ignoreIndex (hash, repo) {
	var result = -1;

	$.each(repo.ignore, function(index, value) {
		if (value === hash) {
			result = index;
			return false;
		}
	});

	return result;
}

// Update the notifiction icon
function updateTotal() {
	var total = 0;
	var orMore = false;

	$.each(repos, function(index, repo) {
		if (repo.unread >= settings.loadLimit)
			orMore = true;
  		total += repo.unread;
	});
	
	unread = total == 0 ? "" : total;
	chrome.browserAction.setBadgeText({text: (unread + (orMore ? "+" : "") ) });
}

// A specific commit is viewed, remove it
function removeCommit (commit, repo) {
	repo.ignore.push(commit.hash);

	if (repo.e != null)
		updateRepository (repo);
}

// Commits are read, remove them
function removeCommits (commits, repo) {
	$.each(commits, function(index, commit) {
		repo.ignore.push(commit.hash);
	});

	if (repo.e != null)
		updateRepository (repo);
}

// All commits of repo are viewed, remove them
function removeAllCommits (repo) {
	repo.last_update = new Date().getTime() / 1000;
	repo.ignore = [];

	if (repo.e != null)
		updateRepository (repo);
}

// If ignore is also the last commit: remove it and update last update
function updateIgnores () {
	$.each(repos, function(index, repo) {
		if (repo.e != null) {
			var values = repo.e.srcElement.response.values;
			
			// Iterate trough commits starting from last
			for (var i=values.length-1; i>=0; i--) {
				commit = values[i];
				var ignoreI = ignoreIndex (commit.hash, repo);
				var date = new Date(commit.date).getTime() / 1000;

				// If it's an unread one (or is in repo.ignore)
				if (date >  repo.last_update) {
					if (ignoreI != -1) {
						repo.ignore.splice (ignoreI, 1);
						repo.last_update = date;

					} else 
						break;
				}
			}
		}
	});
}

// Find and return commit
function findCommit (repo, commitHash) {
	commitOut = undefined;

	$.each(repo.commits, function(index, commit) {
		if (commit.hash === commitHash) {
			commitOut = commit;
			return false;
		}
	});

	return commitOut;
}

// Find and return repo
function findRepo (repoOwner, repoName) {
	var repoOut = undefined;

	$.each(repos, function(index, repo) {
		if (repo.name === repoName && repo.user === repoOwner) {
			repoOut = repo;
			return false;
		}
	});

	return repoOut;
}